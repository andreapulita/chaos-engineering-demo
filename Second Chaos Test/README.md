# Secondo test con Chaos Toolkit

## Prerequisiti
- Python3 versione 3.5+
- Docker o Docker Desktop
- Kind 

## Installazione

### Venv
1. Creare un venv ed attivarlo 
   1. `python3 -m venv <cartella di destinazione>`
2. Installare chaostoolkit usando pip
   1. `pip install chaostoolkit`
3. Installare chaostoolkit per kubernetes usando pip
   1. `pip install chaostoolkit-kubernetes`
4. Copiare il contenuto di questa cartella nella propria oppure utilizzarla direttamente

### Docker service image
1. Creare una immagine di docker utilizzando il contenuto della cartella demo-service
   1. Entrare nella cartella demo-service
   2. `docker build -t chaostest .`

### Kubernetes Cluster usando Kind
1. Creare un cluster con kind utilizzando il file setup.yaml in setup
   1. `kind create cluster --config .\setup\setup.yaml`
2. Utilizzare Ingress-nginx nel cluster per load balancing e gestire accessi ai servizi aspettando che sia pronto all'utilizzo
   1. `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml`
   2. `kubectl wait --namespace ingress-nginx --for=condition=ready pod --selector=app.kubernetes.io/component=controller --timeout=90s`
3. Fare il deploy del servizio effettivo utilizzando i file di configurazione della cartella before\k8s
   1. `kubectl apply -f .\before\k8s\`
4. Controllare che sia tutto in regola con i comandi di gestione pods (3 in stato ErrImage) e nodes (1 control e 3 worker)
   1. `kubectl get nodes`
   2. `kubectl get pods`
5. Caricare l'immagine di docker con il servizio nei nodi
   1. `kind load docker-image chaostest --name chaos`
6. Controllare di nuovo che ora i pods siano tutti pronti per partire ed associati ai nodi
   1. `kubectl get pods -o wide`
7. Ora dovrebbe essere tutto configurato e pronto all'utilizzo
   1. Andare in localhost e localhost/chaos per verificare che tutto funzioni

## Esecuzione senza errore sui pods
1. Far partire il chaos test per trovare vulnerabilità
   1. `chaos run experiment.json`
2. Si noti che i pods vengono comunque ricreati da kubernetes grazie al deployment che ne garantisce sempre 3 disponibili


## Esecuzione con errore sui nodi (manuale)
1. Tiriamo giù uno dei nodi worker ad esempio per manutenzione (cambiare il nome del nodo se serve)
   1. `kubectl drain --force chaos-worker --ignore-daemonsets`
   - Si noti che se tiriamo giù un nodo il pod viene distribuito su un altro nodo
   - Si può controllare sempre con il comando `kubectl get pods -o wide`
   - Se tiriamo giù tutti e tre i nodi worker il servizio non funzionerà più
2. Possiamo riabilitare un nodo con il comando uncordon
   1. `kubectl uncordon chaos-worker`

## Esecuzione senza errore sui nodi (manuale)
1. Per evitare ed avere un numero minimo di nodi e pods attivi si utilizzano i PodDisruptionBudget
2. Applichiamo la regola per avere garantiti 3 pods sempre attivi dentro la cartella after
   1. `kubectl apply -f .\after\`
3. Possiamo vedere che se proviamo a fare il drain di un nodo ora ci viene impedito