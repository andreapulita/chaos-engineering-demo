# Chaos Engineering Demo

In questa cartella sono riportati due test utilizzando il tool Chaos Toolkit.

Per comodità ho messo anche il file requirements.txt con un freeze del mio venv.
Per usare il file basta far partire dentro un venv il comando 

`pip install -r requirements.txt` 

Trovate anche la presentazione in PowerPoint con vari link utili per esplorare ancora di più l'argomento.