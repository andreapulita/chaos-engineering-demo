# Primo test con Chaos Toolkit

## Prerequisiti
Python3 versione 3.5+

## Installazione
- Creare un venv ed attivarlo 
  - `python3 -m venv <cartella di destinazione>`
- Installare chaostoolkit usando pip
  - `pip install chaostoolkit`
- Copiare il contenuto di questa cartella nella propria oppure utilizzarla direttamente

## Esecuzione con errore
- Far partire il servizio service.py
  - `python3 service.py`
- Far partire il chaos test per trovare vulnerabilità
  - `chaos run experiment.json`

## Esecuzione senza errore
- Far partire il servizio resilient-service.py
  - `python3 resilient-service.py`
- Far partire il chaos test per trovare vulnerabilità
  - `chaos run experiment.json`
- Si noti che in questo caso non ci sono errori perchè la mancanza del file viene gestita